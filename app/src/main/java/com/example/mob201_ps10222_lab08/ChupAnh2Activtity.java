package com.example.mob201_ps10222_lab08;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ChupAnh2Activtity extends AppCompatActivity {

    Button btnChup;
    ImageView imgShowHinhAnh;
    private static int chupAnh = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chup_anh2_activtity);
        AnhXa();

        btnChup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, chupAnh);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extra = data.getExtras();
        Bitmap bitmap = (Bitmap) extra.get("data");
        imgShowHinhAnh.setImageBitmap(bitmap);
    }

    private void AnhXa() {
        btnChup = findViewById(R.id.btn_Chup);
        imgShowHinhAnh = findViewById(R.id.img_Show_Anh_Chup);
    }
}
