package com.example.mob201_ps10222_lab08;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

public class QuayPhimActivity extends AppCompatActivity {

    Button btnQuay;
    VideoView videoView_Show;
    private static int quayPhim = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quay_phim);
        AnhXa();

        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null, null, null, null);

        btnQuay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(intent, quayPhim);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extra = data.getExtras();
        Bitmap bitmap = (Bitmap) extra.get("data");
        videoView_Show.setAudioAttributes();
    }

    private void AnhXa() {
        btnQuay = findViewById(R.id.btn_Chup);
        videoView_Show = findViewById(R.id.videoView_Show);
    }
}
