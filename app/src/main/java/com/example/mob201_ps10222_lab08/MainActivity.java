package com.example.mob201_ps10222_lab08;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void click1(View view) {
        Intent intent = new Intent(MainActivity.this, ChupAnhActivity.class);
        startActivity(intent);
    }

    public void click2(View view) {
        Intent intent2 = new Intent(MainActivity.this, MediaActivity.class);
        startActivity(intent2);
    }

    public void click3(View view) {
        Intent intent3 = new Intent(MainActivity.this, ChupAnh2Activtity.class);
        startActivity(intent3);
    }

    public void click4(View view) {
        Intent intent4 = new Intent(MainActivity.this, QuayPhimActivity.class);
        startActivity(intent4);
    }
}
