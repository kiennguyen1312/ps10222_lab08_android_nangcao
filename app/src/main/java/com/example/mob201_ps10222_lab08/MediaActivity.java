package com.example.mob201_ps10222_lab08;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

public class MediaActivity extends AppCompatActivity {

    int UPDATE_FREQUENCY = 500;
    int STEP_VALUE = 4000;
    TextView tv_fileduocchon;
    SeekBar seekBar;
    MediaPlayer player;
    ImageButton btn_play, btn_prev, btn_next;
    ListView lv;
    boolean da_play = true;
    String filehientai = "";
    boolean dichuyenseekbar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        AnhXa();

        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null, null, null, null);
        MyAdapter adapter = new MyAdapter(MediaActivity.this,R.layout.one_tittle, cursor);
        lv.setAdapter(adapter);
    }

    class MyAdapter extends SimpleCursorAdapter{

        public MyAdapter(Context context, int layout, Cursor c) {
            super(context, layout, c, new String[]{MediaStore.MediaColumns.DISPLAY_NAME, MediaStore.MediaColumns.TITLE,
                    MediaStore.Audio.AudioColumns.DURATION},new int[]{R.id.displayName,R.id.title, R.id.duration},
                    FLAG_REGISTER_CONTENT_OBSERVER);
            }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.one_tittle, parent, false);
            bindView(v, context, cursor);
            return v;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView title = (TextView) view.findViewById(R.id.title);
            TextView name = (TextView) view.findViewById(R.id.displayName);
            TextView duration = (TextView) view.findViewById(R.id.duration);
            name.setText(cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME)));
            title.setText(cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.TITLE)));
            long durationInMs = Long.parseLong(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DURATION)));
            double durationInMin = ((double) durationInMs/1000.0/60.0);
            durationInMin = new BigDecimal(Double.toString(durationInMin)).setScale(2,BigDecimal.ROUND_UP).doubleValue();
            duration.setText("" + durationInMin);
            view.setTag(cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA)));
        }
    }

    private void AnhXa() {
        tv_fileduocchon = (TextView) findViewById(R.id.selectedfile);
        btn_play = (ImageButton) findViewById(R.id.play);
        btn_next = (ImageButton) findViewById(R.id.next);
        btn_prev = (ImageButton) findViewById(R.id.prev);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        lv = (ListView) findViewById(R.id.list);
    }
}
