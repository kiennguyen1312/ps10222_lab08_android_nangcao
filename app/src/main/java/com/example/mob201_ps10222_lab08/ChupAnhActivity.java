package com.example.mob201_ps10222_lab08;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ChupAnhActivity extends AppCompatActivity {

    Button btnChup;
    ImageView imgShowHinhAnh;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999 && resultCode == RESULT_OK){
            if (data.getExtras() != null){  // Camera
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imgShowHinhAnh.setImageBitmap(imageBitmap);
            }else {
                Uri uri = data.getData();
                imgShowHinhAnh.setImageURI(uri);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chup_anh);
        AnhXa();
        btnChup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gallery = new Intent(Intent.ACTION_GET_CONTENT);
                gallery.setType("image/*");
                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                Intent chonlua = Intent.createChooser(gallery, "lua chon");
                chonlua.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{camera});
                startActivityForResult(chonlua, 999);
            }
        });
    }

    private void AnhXa() {
        btnChup = findViewById(R.id.btn_Chup);
        imgShowHinhAnh = findViewById(R.id.img_Show_Anh_Chup);
    }
}
